#include "fill.h"
#include <stdint.h>
#include <string.h>

/* assumes RGB565, not optimized, since there are no subsurfaces yet, it does not really make sense to go through rows */
void UG_clear(UG_Surface *dst_surface)
{
    unsigned h = dst_surface->height;
    uint16_t *dst = (uint16_t *)dst_surface->data;

    while (h--) {
        memset(dst, 0, dst_surface->width * 2);

        dst += dst_surface->stride;
    }
}
