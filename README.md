# uCgfx

## What is this?

uCgfx is a minimalistic sprite library for microcontroller class devices,
without any dependencies. There are far better C++ alternatives but I chose
to stick to plain C because of code size on limited flash devices.

This was merely used for my own test purposes and has a lot of flaws. I started
this to target the Nintendo Game And Watch and I have running and unpulished
code on it. Development came to halt after a few days because of a bad Covid
infection and I did not pick up the project again. But I still decided to make
it public and visible.

Limitations:

* Only RGB565 color support
* No input handling, no graphics acceleration, works on raw framebuffers or
  offscreen memory
* No optimized paths, rendering gets slower the more features implemented,
  still this was wicked fast on the target hardware with 100 Sprites on screen
  so I did not care.

## How do I use this

I have some working demo code which uses SDL and code for the Game and Watch
which I might publish also after a cleanup
