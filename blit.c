#include "blit.h"
#include <stdbool.h>
#include <stdint.h>

/* assumes RGB565, not optimized */
void UG_blit(UG_Surface *src_surface, UG_Surface *dst_surface, int x, int y, uint32_t flags, uint16_t mask)
{
    int scalefactor = 1;
    if (flags & UG_FLAG_DOUBLESIZE) {
        scalefactor = 2;
    }

    if (x > dst_surface->width || y > dst_surface->height || x <= -src_surface->width * scalefactor || y <= -src_surface->height * scalefactor) {
        return;
    }

    if (mask == 0) {
        mask = 0xffff;
    }

    unsigned h = src_surface->height * scalefactor;

    if ((y + h) > (dst_surface->height - 1)) {
        h = dst_surface->height - y;
    }

    uint16_t *src = (uint16_t *)src_surface->data;

    if (y < 0) {
        src += -y / scalefactor * src_surface->stride;
        h += y;
        y = 0;
    }

    unsigned width = src_surface->width * scalefactor;

    if ((x + width) > (dst_surface->width - 1)) {
        width = dst_surface->width - x;
    }

    if (x < 0) {
        src += -x / scalefactor;
        width += x;
        x = 0;
    }

    uint16_t *dst = (uint16_t *)dst_surface->data + y * dst_surface->stride + x;

    bool repeated_h = false;
    while (h--) {
        uint16_t *s = src;
        uint16_t *d = dst;

        int w = width;
        bool repeated_w = false;
        if (flags & UG_FLAG_COLORKEY) {
            while (w--) {
                uint16_t pixel = *s;
                if (pixel != src_surface->colorkey) {
                    *d = (pixel & mask);
                }
                d++;
                if (!((flags & UG_FLAG_DOUBLESIZE) && !repeated_w)) {
                    s++;
                }
                repeated_w = !repeated_w;
            }
        }
        else {
            while (w--) {
                *d++ = *s++ & mask;
            }
        }

        if (!((flags & UG_FLAG_DOUBLESIZE) && !repeated_h)) {
            src += src_surface->stride;
        }
        repeated_h = !repeated_h;
        dst += dst_surface->stride;
    }
}
